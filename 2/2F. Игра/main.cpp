#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

vector <int> A[100000];
int s, n, m, length = 0;

vector <int> Win;


int game(int curr)
{

    for (int nxt = 0; nxt < A[curr].size(); nxt++){
            if (Win[A[curr][nxt]] == 0){
                Win[curr] = 1;
                return 1;
            } else if (Win[A[curr][nxt]] == -1) {
                if (game(A[curr][nxt]) == 0){
                    Win[curr] = 1;
                    return 1;
                }
            }
    }
    Win[curr] = 0;
    return 0;
}

int main() {
    ifstream in;
    ofstream out;
    in.open("game.in");
    out.open("game.out");
    in >> n >> m >> s;

    Win.assign (n, 0);
    while (m--) {
        int a, b;
        in >> a >> b;
        a--;
        b--;
        A[a].push_back(b);
        Win[a] = -1;
    }

    if (game(s - 1) == 1){
        out << "First player wins";
    } else {
        out << "Second player wins";
    }

    in.close();
    out.close();
    return 0;
}