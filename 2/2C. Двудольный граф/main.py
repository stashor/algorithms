a1 = open('bipartite.in', 'r')
a2 = open('bipartite.out', 'w')

n, m = map(int, a1.readline().split())
edges = [[] for i in range(n)]

for i in range(m):
    a, b = map(int, a1.readline().split())
    edges[a - 1].append(b - 1)
    edges[b - 1].append(a - 1)

Color = [0] * (n)
IsBipartite = True


def DFS():
    global Color
    global IsBipartite
    global edges

    for i in range(n):
        if Color[i] == 0:
            Color[i] = 1
            s = []
            s.append(i)
            while len(s) > 0:
                v = s.pop(len(s) - 1)
                for i in range(len(edges[v])):
                    if Color[edges[v][i]] == 0:
                        Color[edges[v][i]] = 3 - Color[v]
                        s.append(edges[v][i])
                    elif Color[edges[v][i]] == Color[v]:
                        IsBipartite = False
                        return


DFS()

if IsBipartite:
    a2.write('YES')
else:
    a2.write('NO')

a2.close()
a1.close()