#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

vector <int> G[100010];
vector <int> G_transp[100010];
vector <bool> visited;
vector <int> order, component;
int ans[100010];
int n, m, num = 1;

void dfs(int u)
{
    visited[u] = 1;
    for (int i = 0; i < G[u].size(); i++)
    {
        if (visited[G[u][i]] == 0)
        {
            dfs(G[u][i]);
        }
    }
    order.push_back(u);
}

void dfs_transp(int u)
{
    visited[u] = 1;
    component.push_back(u);
    for (int i = 0; i < G_transp[u].size(); i++)
    {
        if (visited[G_transp[u][i]] == 0)
        {
            dfs_transp(G_transp[u][i]);
        }
    }
}

int main()
{
    ifstream FileIn;
    FileIn.open("cond.in");
    ofstream FileOut;
    FileOut.open("cond.out");

    int a, b;
    FileIn >> n >> m;
    for (int i = 0; i < m; i++)
    {
        FileIn >> a >> b;
        G[a - 1].push_back(b - 1);
        G_transp[b - 1].push_back(a - 1);
    }

    visited.assign(n, false);
    for (int i = 0; i < n; i++)
    {
        if (visited[i] == 0)
            {
                dfs(i);
            }
    }

    visited.assign(n, false);
    for (int i = 0; i < n; i++)
    {
        int v = order[n - 1 - i];
        if (visited[v] == 0)
        {
            dfs_transp(v);
            for (int i = 0; i < component.size(); i++)
            {
                ans[component[i]] = num;
            }
            num++;
            component.clear();
        }
    }

    FileOut << num - 1 << endl;
    for (int i = 0; i < n - 1; i++)
    {
        FileOut << ans[i] << " ";
    }

    FileOut << ans[n - 1];

    FileIn.close();
    FileOut.close();
    return 0;
}