a1 = open('topsort.in', 'r')
a2 = open('topsort.out', 'w')

n, m = map(int, a1.readline().split())
edges = [[] for i in range(n)]

for i in range(m):
    a, b = map(int, a1.readline().split())
    edges[a - 1].append(b - 1)

mark = [0] * n
Ans = []
circle = False


def DFS():
    global Ans
    global circle
    global edges
    global n
    global mark
    s = []
    for i in range(n):
        if mark[i] == 0:
            s.append(i)
            while len(s) > 0:
                v = s[len(s) - 1]
                if mark[v] == 1:
                    mark[v] = 2
                    # Ans.insert(0, s.pop(len(s) - 1))
                    Ans.append(s.pop(len(s) - 1))
                    continue
                elif mark[v] == 2:
                    s.pop(len(s) - 1)
                    continue
                mark[v] = 1
                for i in range(len(edges[v])):
                    if mark[edges[v][i]] == 0:
                        s.append(edges[v][i])
                    elif mark[edges[v][i]] == 1:
                        circle = True
                        return


DFS()
Ans = Ans[::-1]
if circle:
    a2.write('-1')
else:
    s = ''
    for v in Ans:
        s += str(v + 1) + ' '
    a2.write(s)

a2.close()
a1.close()