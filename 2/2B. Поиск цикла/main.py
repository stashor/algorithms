a1 = open('cycle.in', 'r')
a2 = open('cycle.out', 'w')

n, m = map(int, a1.readline().split())
edges = [[] for i in range(n)]

for i in range(m):
    a, b = map(int, a1.readline().split())
    edges[a - 1].append(b - 1)

mark = [0] * n
circle = False
prev = [[] for i in range(n)]
s = []
otv = []


def DFS():
    global circle
    global edges
    global n
    global mark
    global prev
    global s
    global otv

    for i in range(n):
        if mark[i] == 0:
            s = []
            s.append(i)
            while len(s) > 0:
                v = s[len(s) - 1]
                if mark[v] == 1:
                    mark[v] = 2
                    s.pop(len(s) - 1)
                    continue
                mark[v] = 1
                for i in range(len(edges[v])):
                    if mark[edges[v][i]] == 0:
                        prev[edges[v][i]] = v
                        s.append(edges[v][i])
                    elif mark[edges[v][i]] == 1:
                        circle = True
                        k = v
                        otv.append(k)
                        while k != edges[v][i]:
                            k = prev[k]
                            otv.append(k)
                        otv = otv[::-1]
                        return


DFS()

if circle:
    a2.write('YES\n')
    st = ''
    for v in otv:
        st += str(v + 1) + ' '
    a2.write(st)
else:
    a2.write('NO')

a2.close()
a1.close()