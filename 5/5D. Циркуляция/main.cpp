#include <iostream>
#include <fstream>
#include <vector>
#include <queue>

using namespace std;

int n, m;
vector <vector <int>> edgeflow;
vector <vector <int>> throughput;

bool BFS(){
    int u, dest = n - 1;
    vector <int> prev(n);
    prev.assign(n, INT_MAX);
    prev[0] = 0;
    queue<int> q;
    q.push(0);
    while (q.size() > 0){
        u = q.front();
        q.pop();
        for (int v = 0; v < n; v++){
            if (prev[v] == INT_MAX && throughput[u][v] - edgeflow[u][v] > 0){
                prev[v] = u;
                if (v == dest){
                    int w = v;
                    int delta = INT_MAX;
                    for (u = prev[w]; v != 0; v = u, u = prev[u]){
                        delta = min(delta, throughput[u][v] - edgeflow[u][v]);
                    }
                    for (u = prev[w], v = w; v != 0; v = u, u = prev[u]){
                        edgeflow[u][v] += delta;
                        edgeflow[v][u] -= delta;
                    }
                    return delta;
                }
                q.push(v);
            }
        }
    }
    return 0;

}

int main() {
    ifstream in;
    ofstream out;
    in.open("circulation.in");
    out.open("circulation.out");

    in >> n >> m;
    n = n + 2;

    for (int i = 0; i < n; i++) {
        vector <int> tmp1;
        tmp1.assign(n, 0);
        edgeflow.push_back(tmp1);
        vector <int> tmp2;
        tmp2.assign(n, 0);
        throughput.push_back(tmp2);
    }

    int edgenumbers[m][2];
    int mincap[n][n];
    int u, v;
    int s = 0, d = n - 1;
    int min, max;
    for (int i = 0; i < m; i++) {
        in >> u >> v >> min >> max;
        throughput[s][v] += min;
        throughput[u][v] = max - min;
        throughput[u][d] += min;
        edgenumbers[i][0] = u;
        edgenumbers[i][1] = v;
        mincap[u][v] = min;
    }

    while (BFS() > 0);

    for (int i = 1; i < n; i++) {
        if (edgeflow[s][i] < throughput[s][i]){
            out << "NO";
            in.close();
            out.close();
            return 0;
        }
    }

    out << "YES\n" ;
    for (int i = 0; i < m; i++) {
        u = edgenumbers[i][0];
        v = edgenumbers[i][1];
        out << edgeflow[u][v] + mincap[u][v] << "\n";
    }

    in.close();
    out.close();
    return 0;
}