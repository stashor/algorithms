#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

int n, m, s, f, w;

int dfs(int u, int &dst, vector < vector <int> > &g, vector <bool> &visited, int flow){

    if (u == dst)
        return flow;

    visited[u] = true;

    for (int i = 0; i < n; i++){
        if (g[u][i] > 0 && !visited[i]){
            int delta = dfs(i, dst, g, visited, min(flow, g[u][i]));
            if (delta > 0){
                g[u][i] -= delta;
                g[i][u] += delta;
                return delta;
            }
        }
    }
    return 0;
}

int main() {
    ifstream in;
    ofstream out;
    in.open("maxflow.in");
    out.open("maxflow.out");

    in >> n >> m;

    vector < vector <int> > g;

    for (int i = 0; i < n; i++) {
        vector <int> tmp;
        tmp.assign(n, 0);
        g.push_back(tmp);
    }

    for (int i = 0; i < m; i++) {
        in >> s >> f >> w;
        g[s - 1][f - 1] = w;
    }

    int dst = n - 1;
    vector <bool> visited(n);
    int maxflow = 0;
    int curflow = 1;

    while (curflow > 0){
        visited.assign(n, false);
        curflow = dfs(0, dst, g, visited, INT_MAX);
        maxflow += curflow;
    }

    out << maxflow;

    in.close();
    out.close();
    return 0;
}