#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

int n, m, k, s, f, w;

bool kuhn(int u, vector < vector <bool> > &g, vector <bool> &visited, vector <int> &matching){

    if (visited[u])
        return false;
    visited[u] = true;

    for (int i = 0; i < m; i++){
        if (g[u][i]){
            if (matching[i] == INT_MAX || kuhn(matching[i], g, visited, matching)){
                matching[i] = u;
                return true;
            }
        }
    }
    return false;
}

int main() {
    ifstream in;
    ofstream out;
    in.open("matching.in");
    out.open("matching.out");

    in >> n >> m >> k;

    vector < vector <bool> > g;

    for (int i = 0; i < n; i++) {
        vector <bool> tmp;
        tmp.assign(m, false);
        g.push_back(tmp);
    }

    for (int i = 0; i < k; i++) {
        in >> s >> f;
        g[s - 1][f - 1] = true;
    }

    vector <bool> visited(n);
    vector <int> matching(m);
    matching.assign(m, INT_MAX);
    int count = 0;

    for (int v = 0; v < n; v++) {
        for (int i = 0; i < n; i++) {
            visited[i] = false;
        }
        if (kuhn(v, g, visited, matching))
            count++;
    }

    out << count;

    in.close();
    out.close();
    return 0;
}