#include <iostream>
#include <fstream>
#include <vector>
#include <queue>

using namespace std;

class Edge{
public: int end, number, flow, throughput, ri;
public:Edge(int end, int number, int throughput, int ri){
        this->end = end;
        this->number = number;
        this->flow = 0;
        this->throughput = throughput;
        this->ri = ri;
    }
};

vector<vector<Edge>> Edges;
vector<int> distances;
vector<int> ptr;
int n, dest;
vector<Edge> path;

bool BFS(){
    int u;
    distances.assign(n, INT_MAX);
    distances[0] = 0;
    queue<int> queue;
    queue.push(0);
    while (queue.size() > 0){
        u = queue.front();
        queue.pop();
        for (int i = 0; i < Edges[u].size(); ++i) {
            if (distances[Edges[u][i].end] == INT_MAX && Edges[u][i].flow < Edges[u][i].throughput){
                queue.push(Edges[u][i].end);
                distances[Edges[u][i].end] = distances[u] + 1;
            }
        }
    }
    return distances[dest] != INT_MAX;
}

int DFS(int u, int flow){
    if (flow == 0 || u == dest)
        return flow;
    for ( ; ptr[u] < Edges[u].size(); ++ptr[u]){
        if (distances[Edges[u][ptr[u]].end] == distances[u] + 1 && Edges[u][ptr[u]].flow < Edges[u][ptr[u]].throughput){
            int diff = Edges[u][ptr[u]].throughput - Edges[u][ptr[u]].flow;
            int delta = DFS(Edges[u][ptr[u]].end, min(flow, diff));
            if (delta > 0){
                Edges[u][ptr[u]].flow += delta;
                Edges[Edges[u][ptr[u]].end][Edges[u][ptr[u]].ri].flow -= delta;
                return delta;
            }
        }
    }
    return 0;
}

int GetPathDFS(int u, int flow){
    if (u == dest)
        return flow;
    for (int i = 0; i < Edges[u].size(); ++i) {
        if (Edges[u][i].flow > 0){
            path.push_back(Edges[u][i]);
            flow = GetPathDFS(Edges[u][i].end, min(flow, Edges[u][i].flow));
            Edges[u][i].flow -= flow;
            return flow;
        }
    }
    return 0;
}

int main() {
    ifstream in;
    ofstream out;
    in.open("decomposition.in");
    out.open("decomposition.out");

    int m;
    in >> n >> m;

    for (int i = 0; i < n; ++i) {
        vector<Edge> tmp;
        Edges.push_back(tmp);
    }

    int u, v, f;
    for (int i = 1; i <= m; ++i) {
        in >> u >> v >> f;
        u--;
        v--;
        if (f > 0){
            Edges[u].push_back(Edge(v, i, f, Edges[v].size()));
            Edges[v].push_back(Edge(u, 0, 0, Edges[u].size() - 1));
        }
    }

    dest = n - 1;
    int curflow = 1;
    while (BFS()){
        ptr.assign(n, 0);
        do curflow = DFS(0, INT_MAX);
        while (curflow > 0);
    }

    int pathscount = 0;
    string answer = "";
    while (true){
        curflow = GetPathDFS(0, INT_MAX);
        if (curflow == 0)
            break;
        ++pathscount;
        answer.append(to_string(curflow)).append(" ");
        answer.append(to_string(path.size())).append(" ");
        for (int i = 0; i < path.size(); ++i) {
            answer.append(to_string(path[i].number)).append(" ");
        }
        answer.append("\n");
        path.clear();
    }

    out << pathscount << "\n";
    out << answer;

    in.close();
    out.close();
    return 0;
}