def prefix(s):
    v = [0] * len(s)
    for i in range(1, len(s)):
        k = v[i - 1]
        while k > 0 and s[k] != s[i]:
            k = v[k - 1]
        if s[k] == s[i]:
            k = k + 1
        v[i] = k
    return v


a1 = open('prefix.in', 'r')
a2 = open('prefix.out', 'w')

s = a1.readline().strip()

a2.write(' '.join(map(str, prefix(s))))

a2.close()
a1.close()