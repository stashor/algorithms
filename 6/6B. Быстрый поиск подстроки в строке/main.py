a1 = open('search2.in', 'r')
a2 = open('search2.out', 'w')

p = a1.readline().strip()
t = p + '##' + a1.readline().strip()

pref = 0
prefs = [0] * len(t)
count = 0
ans = []

for i in range(1, len(t)):
    while pref > 0 and t[pref] != t[i]:
        pref = prefs[pref - 1]
    if t[pref] == t[i]:
        pref += 1
    prefs[i] = pref;
    if pref == len(p):
        count += 1
        ans.append(str(i - 2 * len(p)) + ' ')

a2.write(str(count) + '\n')
answer = ''.join(ans)
a2.write(answer)

a2.close()
a1.close()