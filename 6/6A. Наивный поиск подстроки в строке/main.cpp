a1 = open('search1.in', 'r')
a2 = open('search1.out', 'w')

p = a1.readline().strip()
t = a1.readline().strip()

st = ''
c = 0
s = 0
flag = True
while flag:
    pos = t.find(p, s)
    if pos != -1:
        st += str(pos + 1) + ' '
        s = pos + 1
        c += 1
    else:
        flag = False

a2.write(str(c) + '\n')
a2.write(st)

a2.close()
a1.close()