import copy

a1 = open('input.txt', 'r')
a2 = open('output.txt', 'w')

n, m = map(int, a1.readline().split())
a = []
for i in range(n):
    a.append([])

for i in range(n):
    s = a1.readline()
    for j in range(len(s)):
        if s[j] == '.':
            a[i].append(0)
        if s[j] == '#':
            a[i].append(-1)
        if s[j] == 'S':
            a[i].append(0)
            st1 = i
            st2 = j
        if s[j] == 'T':
            a[i].append(0)
            en1 = i
            en2 = j

a[st1][st2] = 1
d = 1
wave = [[st1, st2]]
newwave = []
while a[en1][en2] == 0 and len(wave) != 0:
    for x in wave:
        i = x[0]
        j = x[1]
        if i - 1 >= 0:
            if a[i - 1][j] == 0:
                a[i - 1][j] = d + 1
                newwave.append([i - 1, j])
        if j - 1 >= 0:
            if a[i][j - 1] == 0:
                a[i][j - 1] = d + 1
                newwave.append([i, j - 1])
        if i + 1 <= n - 1:
            if a[i + 1][j] == 0:
                a[i + 1][j] = d + 1
                newwave.append([i + 1, j])
        if j + 1 <= m - 1:
            if a[i][j + 1] == 0:
                a[i][j + 1] = d + 1
                newwave.append([i, j + 1])
    wave = []
    wave = copy.copy(newwave)
    newwave = []
    d += 1

if a[en1][en2] == 0:
    a2.write(str(-1))
else:
    a2.write(str(d - 1) + '\n')
    path = ''
    x = en1
    y = en2

    while d != 0:
        f = 1
        if x - 1 >= 0 and f == 1:
            if a[x - 1][y] == (d - 1):
                path = 'D' + path
                x -= 1
                f = 0
        if y - 1 >= 0 and f == 1:
            if a[x][y - 1] == d - 1:
                path = 'R' + path
                y -= 1
                f = 0
        if x + 1 <= n - 1 and f == 1:
            if a[x + 1][y] == d - 1:
                path = 'U' + path
                x += 1
                f = 0
        if y + 1 <= m - 1 and f == 1:
            if a[x][y + 1] == d - 1:
                path = 'L' + path
                y += 1
                f = 0
        d -= 1

    a2.write(path)

a2.close()
a1.close()