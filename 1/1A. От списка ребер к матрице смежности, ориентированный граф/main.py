a1 = open('input.txt', 'r')
a2 = open('output.txt', 'w')

n, m = map(int, a1.readline().split())
a = []
for i in range(n):
    a.append([0] * n)

for i in range(m):
    k, p = map(int, a1.readline().split())
    a[k - 1][p - 1] = 1

for i in a:
    s = ''
    for j in i:
        s += str(j) + ' '
    a2.write(s + '\n')

a2.close()
a1.close()