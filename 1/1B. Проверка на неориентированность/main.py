a1 = open('input.txt', 'r')
a2 = open('output.txt', 'w')

n = int(a1.readline())
a = []
for i in range(n):
    a.append(list(map(int, a1.readline().split())))

f = 0
for i in range(n):
    for j in range(n):
        if a[i][j] != a[j][i] or a[i][i] == 1:
            f = 1

if f == 0:
    a2.write('YES')
else:
    a2.write('NO')
a2.close()
a1.close()