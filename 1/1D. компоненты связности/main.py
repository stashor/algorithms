a1 = open('components.in', 'r')
a2 = open('components.out', 'w')

n, m = map(int, a1.readline().strip().split())
a = []
for i in range(n):
    a.append([])

for i in range(m):
    k, p = map(int, a1.readline().strip().split())
    a[k - 1].append(p - 1)
    a[p - 1].append(k - 1)

count = 0
svyzn = [0] * n
stack = []
kol = 0
for v in range(n):
    if kol == n:
        break
    if svyzn[v] == 0:
        count += 1
        svyzn[v] = count
        kol += 1
        stack.append(v)
        while len(stack) != 0:
            el = stack.pop()
            for i in a[el]:
                if svyzn[i] == 0:
                    stack.append(i)
                    svyzn[i] = count
                    kol += 1

a2.write(str(count) + '\n')
a2.write(' '.join(map(str, svyzn)))

a2.close()
a1.close()