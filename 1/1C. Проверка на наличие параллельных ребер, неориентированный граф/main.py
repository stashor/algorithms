a1 = open('input.txt', 'r')
a2 = open('output.txt', 'w')

n, m = map(int, a1.readline().split())
a = []
for i in range(n):
    a.append([0] * n)

f = 0
for i in range(m):
    k, p = map(int, a1.readline().split())
    if a[p - 1][k - 1] == 1:
        f = 1
        break
    a[k - 1][p - 1] = 1
    a[p - 1][k - 1] = 1

if f == 0:
    a2.write('NO')
else:
    a2.write('YES')

a2.close()
a1.close()