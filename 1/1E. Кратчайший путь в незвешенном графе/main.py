def bfs(s):
    global level
    level[s] = 0
    stack = [s]
    while stack:
        v = stack.pop(0)
        for w in a[v]:
            if level[w] is -1:
                stack.append(w)
                level[w] = level[v] + 1


a1 = open('pathbge1.in', 'r')
a2 = open('pathbge1.out', 'w')

n, m = map(int, a1.readline().split())
a = []
for i in range(n):
    a.append([])

for i in range(m):
    k, p = map(int, a1.readline().split())
    a[k - 1].append(p - 1)
    a[p - 1].append(k - 1)

level = [-1] * n
for i in range(n):
    if level[i] is -1:
        bfs(i)

a2.write(' '.join(map(str, level)))

a2.close()
a1.close()