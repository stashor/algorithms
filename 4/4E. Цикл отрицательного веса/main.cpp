#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

const long long INF = 1000000000;

int n, s;

int main() {
    ifstream in;
    ofstream out;
    in.open("negcycle.in");
    out.open("negcycle.out");

    in >> n;

    vector < vector <int> > g(n);

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            in >> s;
            g[i].push_back(s);
        }
    }

    vector<long long> d(n, INF);
    vector<int> p(n);
    d[0] = 0;

    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (d[j] > d[i] + g[i][j]){
                    d[j] = d[i] + g[i][j];
                    p[j] = i;
                }
            }
        }
    }

    int c = INT_MAX;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (d[j] > d[i] + g[i][j]){
                c = j;
                break;
            }
        }
    }

    if (c != INT_MAX){
        vector <int> cycle;
        for (int i = 0; i < n - 1; i++)
            c = p[c];

        cycle.push_back(c);
        for (int v = p[c]; v != c; v = p[v])
            cycle.push_back(v);
        cycle.push_back(c);
        reverse(cycle.begin(), cycle.end());
        out << "YES" << "\n";
        out << cycle.size() << "\n";
        for (int i = 0; i < cycle.size(); i++)
            out << cycle[i] + 1 << " ";
    } else {
        out << "NO";
    }



    in.close();
    out.close();
    return 0;
}