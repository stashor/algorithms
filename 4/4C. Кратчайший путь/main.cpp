#include <iostream>
#include <fstream>
#include <vector>
#include <queue>

using namespace std;

const int INF = INT_MAX;

int n, m, s, e, w;

int main() {
    ifstream in;
    ofstream out;
    in.open("pathbgep.in");
    out.open("pathbgep.out");

    in >> n >> m;

    vector < vector < pair<int, int> > > g (n);

    for (int i = 0; i < m; ++i) {
        in >> s >> e >> w;
        g[s - 1].push_back(make_pair(e - 1, w));
        g[e - 1].push_back(make_pair(s - 1, w));
    }
    s = 0;

    vector<int> d (n, INF),  p (n);
    d[s] = 0;
    priority_queue < pair<int,int> > q;
    q.push (make_pair (0, s));
    while (!q.empty()) {
        int v = q.top().second,  cur_d = -q.top().first;
        q.pop();
        if (cur_d > d[v])  continue;

        for (size_t j=0; j<g[v].size(); ++j) {
            int to = g[v][j].first,
                    len = g[v][j].second;
            if (d[v] + len < d[to]) {
                d[to] = d[v] + len;
                p[to] = v;
                q.push (make_pair (-d[to], to));
            }
        }
    }

    for (int i = 0; i < n; ++i) {
        out << d[i] << " ";
    }

    in.close();
    out.close();
    return 0;
}