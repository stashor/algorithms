#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

vector <vector <long long>> graph;
vector <long long> d;
vector <bool> u;
long long n, m, t, s, f, w;

int main() {
    ifstream in;
    ofstream out;
    in.open("pathsg.in");
    out.open("pathsg.out");

    in >> n >> m;

    for (int i = 0; i < n; i++) {
        vector<long long> tmp;
        for (int j = 0; j < n; j++) {
            t = 1000000;
            tmp.push_back(t);
        }
        graph.push_back(tmp);
    }

    for (int i = 0; i < m; i++) {
        in >> s >> f >> w;
        graph[s - 1][f - 1] = w;
    }

    for (int i = 0; i < n; i++) {
        graph[i][i] = 0;
    }

    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (graph[i][j] > graph[i][k] + graph[k][j])
                    graph[i][j] = graph[i][k] + graph[k][j];
            }
        }
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            out << graph[i][j] << " ";
        }
        out << "\n";
    }

    in.close();
    out.close();
    return 0;
}