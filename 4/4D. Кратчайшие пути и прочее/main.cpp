#include <iostream>
#include <fstream>
#include <vector>
#include <limits>

using namespace std;

const double INF = (double)ULLONG_MAX;

long long n, m, s, e, b;
long long w;

vector <vector <long long>> Edges;

void DFS(vector <vector <long long>> &Edges, vector <bool> &been, int v){
    been[v] = true;
    for (int i = 0; i < Edges.size(); i++){
        if (Edges[i][0] == v && !been[Edges[i][1]]){
            DFS(Edges, been, Edges[i][1]);
        }
    }
}

int main() {
    ifstream in;
    ofstream out;
    in.open("path.in");
    out.open("path.out");

    in >> n >> m >> s;
    s = s - 1;
    for (int i = 0; i < m; i++) {
        in >> b >> e >> w;
        vector <long long> tmp;
        tmp.push_back(b - 1);
        tmp.push_back(e - 1);
        tmp.push_back(w);
        Edges.push_back(tmp);
    }

    vector<double> d (n, INF);
    d[s] = 0;

    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < m; j++) {
            if (d[Edges[j][0]] < INF){
                if (d[Edges[j][1]] > d[Edges[j][0]] + (double)Edges[j][2])
                d[Edges[j][1]] = d[Edges[j][0]] + (double)Edges[j][2];
            }
        }
    }

    vector<bool> neg(n, false);
    for (int j = 0; j < m; j++) {
        if (d[Edges[j][0]] < INF){
            if (d[Edges[j][1]] > d[Edges[j][0]] + (double)Edges[j][2])
                if (!neg[Edges[j][1]])
                    DFS(Edges, neg, Edges[j][1]);
        }
    }

    for (int i = 0; i < n; ++i) {
        if (d[i] == INF)
            out << "*" << "\n";
        else if (neg[i])
            out << "-" << "\n";
        else
            out << (long long)d[i] << "\n";
    }

    in.close();
    out.close();
    return 0;
}