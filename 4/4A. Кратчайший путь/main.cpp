#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <climits>

using namespace std;

vector <vector <long long>> graph;
vector <long long> d;
vector <bool> u;
long long n, s, f, mini;

int main() {
    ifstream in;
    ofstream out;
    in.open("pathmgep.in");
    out.open("pathmgep.out");

    in >> n >> s >> f;
    s--;
    f--;

    d.assign(n, LLONG_MAX);
    u.assign(n, false);

    long long t;

    for (int i = 0; i < n; i++) {
        vector<long long> tmp;
        for (int j = 0; j < n; j++) {
            in >> t;
            tmp.push_back(t);
        }
        graph.push_back(tmp);
    }

    d[s] = 0;
    long long p = 0;
    for (int c = 0; c < n; c++) {
        mini = LLONG_MAX;
        for (int i = 0; i < n; i++) {
            if (!u[i] && d[i] < mini){
                mini = d[i];
                p = i;
            }
        }
        u[p] = true;
        for (int i = 0; i < n; i++) {
            if (!u[i] && graph[p][i] != -1 && d[p] != INT_MAX && d[p] + graph[p][i] < d[i])
                d[i] = d[p] + graph[p][i];
        }
    }

    if (d[f] != LLONG_MAX){
        out << d[f];
    } else {
        out << -1;
    }

    in.close();
    out.close();
    return 0;
}