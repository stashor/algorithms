#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <iomanip>

using namespace std;

vector <bool> outs;
vector <int> dst;
int n;
int pointX[5000];
int pointY[5000];

int main() {
    ifstream in;
    ofstream out;
    in.open("spantree.in");
    out.open("spantree.out");

    in >> n;
    for (int i = 0; i < n; i++){
        in >> pointX[i] >>  pointY[i];
    }

    outs.assign(n, true);
    dst.assign(n, 0);
    for (int i = 0; i < n; i++){
        dst[i] = (pointX[0] - pointX[i]) * (pointX[0] - pointX[i]) + (pointY[0] - pointY[i]) * (pointY[0] - pointY[i]);
    }

    double path = 0;
    int pt = 0;
    int d;
    int count = 0;
    while (count < n) {
        int minpath = INT_MAX;
        pt = INT_MAX;
        for (int i = 0; i < n; i++){
            if (outs[i] && dst[i] < minpath){
                minpath = dst[i];
                pt = i;
            }
        }
        count++;
        outs[pt] = false;
        path += sqrt(minpath);
        for (int i = 0; i < n; i++){
            if (outs[i]){
                d = (pointX[pt] - pointX[i]) * (pointX[pt] - pointX[i]) + (pointY[pt] - pointY[i]) * (pointY[pt] - pointY[i]);
                if (d < dst[i])
                    dst[i] = d;
            }
        }
    }

    out << setprecision(15) << path;

    in.close();
    out.close();
    return 0;
}