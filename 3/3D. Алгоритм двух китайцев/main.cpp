#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

struct Edge {
    int end;
    int weight;
};

vector <vector <Edge>> Edges;
int n, m;

void DFS(int v, vector <vector <Edge>> &Edges, vector <bool> &color){
    color[v] = true;
    for (int i = 0; i < Edges[v].size(); i++){
        if (!color[Edges[v][i].end]){
            DFS(Edges[v][i].end, Edges, color);
        }
    }
}

bool CheckAccessible(int root, vector <vector <Edge>> &Edges, int n){
    vector <bool> color;
    color.assign(n + 1, false);
    DFS(root, Edges, color);
    for (int i = 1; i <= n; i++){
        if (color[i] == false)
            return false;
    }
    return true;
}

struct Components {
    vector <int> comp;
    int comp_n;
};

void DFS1(int v, vector <vector <Edge>> &Edges, vector <bool> &color, vector <int> &ord){
    color[v] = true;
    for (int i = 0; i < Edges[v].size(); i++){
        if (!color[Edges[v][i].end]){
            DFS1(Edges[v][i].end, Edges, color, ord);
        }
    }
    ord.push_back(v);
}

void DFS2(int v, vector <vector <Edge>> &inverted, vector <int> &comp, int comp_n){
    comp[v] = comp_n;
    for (int i = 0; i < inverted[v].size(); i++){
        if (comp[inverted[v][i].end] == 0){
            DFS2(inverted[v][i].end, inverted, comp, comp_n);
        }
    }
}

Components Condensation(vector <vector <Edge>> Edges, int n){
    vector <bool> color;
    color.assign(n + 1, false);
    Components c;
    c.comp.assign(n + 1, 0);
    c.comp_n = 0;
    vector <int> ord;
    for (int i = 1; i <= n; i++) {
        if (!color[i]){
            DFS1(i, Edges, color, ord);
        }
    }
    vector <vector <Edge>> inverted;
    for (int i = 0; i < n + 1; i++) {
        vector <Edge> tmp;
        inverted.push_back(tmp);
    }

    for (int v = 1; v <= n; v++) {
        for (int i = 0; i < Edges[v].size(); i++){
            Edge tmp;
            tmp.end = v;
            tmp.weight = Edges[v][i].weight;
            inverted[Edges[v][i].end].push_back(tmp);

        }
    }
    reverse(ord.begin(), ord.end());
    for (int v = 0; v < ord.size(); v++) {
        if (c.comp[ord[v]] == 0)
            DFS2(ord[v], inverted, c.comp, ++c.comp_n);
    }
    return c;
}

long long FindMST(vector <vector <Edge>> Edges, int n, int root){
    long long res = 0;
    if (!CheckAccessible(root, Edges, n))
        return LONG_LONG_MIN;
    int minEdges[n + 1];
    for (int i = 0; i < n + 1; i++) {
        minEdges[i] = INT_MAX;
    }
    for (int v = 1; v <= n; v++) {
        for (int i = 0; i < Edges[v].size(); i++){
            if (Edges[v][i].weight < minEdges[Edges[v][i].end])
                minEdges[Edges[v][i].end] = Edges[v][i].weight;
        }
    }
    for (int v = 1; v <= n; v++) {
        if (v != root)
            res += minEdges[v];
    }
    vector <vector <Edge>> zeroEdges;
    for (int i = 0; i < n + 1; i++) {
        vector <Edge> tmp;
        zeroEdges.push_back(tmp);
    }
    for (int v = 1; v <= n; v++) {
        for (int i = 0; i < Edges[v].size(); i++){
            if (Edges[v][i].weight == minEdges[Edges[v][i].end]){
                Edge tmp;
                tmp.end = Edges[v][i].end;
                tmp.weight = 0;
                zeroEdges[v].push_back(tmp);
            }
        }
    }
    if (CheckAccessible(root, zeroEdges, n))
        return res;
    Components c = Condensation(zeroEdges, n);
    vector <vector <Edge>> newEdges;
    for (int i = 0; i < c.comp_n + 1; i++) {
        vector <Edge> tmp;
        newEdges.push_back(tmp);
    }
    for (int v = 1; v <= n; v++) {
        for (int i = 0; i < Edges[v].size(); i++){
            if (c.comp[v] != c.comp[Edges[v][i].end]){
                Edge tmp;
                tmp.end = c.comp[Edges[v][i].end];
                tmp.weight = Edges[v][i].weight - minEdges[Edges[v][i].end];
                newEdges[c.comp[v]].push_back(tmp);
            }
        }
    }
    res += FindMST(newEdges, c.comp_n, c.comp[root]);
    return res;
}

int main() {
    ifstream in;
    ofstream out;
    in.open("chinese.in");
    out.open("chinese.out");

    in >> n >> m;

    for (int i = 0; i < n + 1; i++) {
        vector <Edge> tmp;
        Edges.push_back(tmp);
    }

    int start, end, weight;
    for (int i = 0; i < m; i++) {
        in >> start >> end >> weight;
        Edge tmpEdge;
        tmpEdge.end = end;
        tmpEdge.weight = weight;
        Edges[start].push_back(tmpEdge);
    }

    long long mst = FindMST(Edges, n, 1);

    if (mst != LONG_LONG_MIN){

        out << "YES\n";
        out << mst;
    } else {
        out << "NO";
    }

    in.close();
    out.close();
    return 0;
}