a1 = open('input.txt', 'r')
a2 = open('output.txt', 'w')

n, m = map(int, a1.readline().split())

a = [0] * n

for i in range(m):
    k, p = map(int, a1.readline().split())
    a[k - 1] += 1
    a[p - 1] += 1

s = ''
for i in a:
    s += str(i) + ' '

a2.write(s)

a2.close()
a1.close()