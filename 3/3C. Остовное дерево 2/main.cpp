#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <iomanip>
#include <algorithm>

using namespace std;

vector <int> Comp;
vector <vector <int>> Edges;
int n, m, x, y, w, Ans = 0, a, b;

int main() {
    ifstream in;
    ofstream out;
    in.open("spantree2.in");
    out.open("spantree2.out");

    in >> n >> m;
    for (int i = 0; i < m; i++){
        in >> x >>  y >> w;
        vector<int> tmpVec;
        tmpVec.push_back(x - 1);
        tmpVec.push_back(y - 1);
        tmpVec.push_back(w);
        Edges.push_back(tmpVec);
    }

    sort(Edges.begin(), Edges.end(), [](const vector<int>& a, const vector<int>& b) {return a[2] < b[2];});

    for (int i = 0; i < n; i++){
        Comp.push_back(i);
    }

    for (int i = 0; i < m; i++){
        int weight = Edges[i][2];
        int start = Edges[i][0];
        int end = Edges[i][1];
        if (Comp[start] != Comp[end]){
            Ans += weight;
            a = Comp[start];
            b = Comp[end];
            for (int i = 0; i < n; i++){
                if (Comp[i] == b){
                    Comp[i] = a;
                }

            }
        }
    }

    out << Ans;

    in.close();
    out.close();
    return 0;
}